import datetime
from pymongo import MongoClient, DESCENDING
import pprint

client = MongoClient("mongodb://localhost:27017/")

def get_latest():
    # Selects the DB
    db = client.test

    # Selects collection
    entries = db.entries

    # Query for last document entry and converts the cursor to a list
    ents = list(entries.find({}).sort('_id', DESCENDING).limit(1))

    # If we have any results
    if len(ents) > 0:
        # select first
        ent = ents[0]
        
        # uncomment here if you want to see the selected document
        # pprint.pprint(ent)

        # picks the date field to operate on
        admision_date = ent["StudentAdmissionDate"]
        # gets current time, as reference
        now = datetime.datetime.now()
        # if current time is greater that the selected time, calculates the
        # minutes passed
        if now > admision_date:
            # time difference
            diff = now - admision_date
            # converts to minutes, from seconds
            to_mins = diff.total_seconds() / 60
            # see output
            print("=====> time now is greater", admision_date, now, diff, to_mins)


get_latest()
