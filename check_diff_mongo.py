import datetime
from pymongo import MongoClient, DESCENDING
import pprint
import os
import sys

client = MongoClient("mongodb://localhost:27093/")

def get_latest():
    # pick DB
    db = client.test
    # pick collection
    entries = db.addCurrentDateTimeDemo
    # do query
    ents = list(entries.find({}).sort('_id', DESCENDING).limit(1))
    if len(ents) > 0:
        ent = ents[0]
        #pprint.pprint(ent)
        admision_date = ent["StudentAdmissionDate"]
        now = datetime.datetime.now()
        if now < admision_date:
            diff = now - admision_date
            #print(admision_date, now, diff, (diff.total_seconds() / 60))
            print("Hay registros con mas de 10 minutos de diferencia")
            sys.exit(0)
        print ("No hay registros con mas de 10 minutos de diferencia")
        pprint.pprint(ent)
        sys.exit(2)
get_latest()
